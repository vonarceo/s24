console.log("Hello World!")

const number = 2
const getCube = (anyNum) => {
	return number ** 3;
}

console.log(`The cube of ${number} is ${getCube(number)}`);

const address = ['Paranaque', 'Field Residences', 'Philippines'];

const [city, street, country] = address;
console.log(`I live at ${street} ${city} ${country}`);

const animal = {
	type: 'Dog',
	weight: '300',
	height: '5 feet and 2 inches'
};

const {type, weight, height} = animal;

console.log(`Doggo was a ${type}. he weighted at ${weight} with a measurement of ${height}`);


const numArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

numArray.forEach((number) => console.log(number));

const reduceNumber = numArray.reduce((num1, num2) => {return num1 + num2});

console.log(reduceNumber);

